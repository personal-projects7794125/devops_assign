# nginx Deployment on Kubernetes

This repository contains Kubernetes manifests for deploying nginx with various configurations like Horizontal Pod Autoscaler, Pod Disruption Budget, Secrets, Service, and Service Account.

## Prerequisites

Ensure you have the following installed:

- Kubernetes cluster
- `kubectl` command-line tool configured to connect to your cluster

## Deployment Steps

1. **Deploy nginx using Deployment**

   Apply the deployment YAML to create the nginx deployment:

   ```bash
   kubectl apply -f deployment.yaml
   
2. **Set up Horizontal Pod Autoscaler (HPA)**

   Apply the HPA YAML to configure autoscaling based on CPU utilization:

   ```bash
   kubectl apply -f hpa.yaml
   
3. **Create Pod Disruption Budget (PDB)**

   Apply the PDB YAML to define the minimum number of pods available during disruptions:

   ```bash
   kubectl apply -f pdb.yaml
   
4. **Create API Key Secret**

   Apply the Secret YAML to store the API key securely:

   ```bash
   kubectl apply -f secret.yaml
   
5. **Create Service Account**

   Apply the Service Account YAML to provide necessary permissions:

   ```bash
   kubectl apply -f service-account.yaml
   
6. **Expose nginx Deployment as a Service**

   Apply the Service YAML to expose nginx using a NodePort service:

   ```bash
   kubectl apply -f service.yaml   

## Clean up

To delete all resources created by these manifests, use the following commands:

   ```bash
   kubectl delete deployment nginx-deployment
   kubectl delete hpa nginx-hpa
   kubectl delete pdb nginx-pdb
   kubectl delete secret api-key-secret
   kubectl delete service nginx-service
   kubectl delete serviceaccount nginx-sa